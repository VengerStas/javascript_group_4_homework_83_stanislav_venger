const mongoose = require('mongoose');
const config = require('./config');

const Artist = require('./models/Artist');
const Album = require('./models/Album');
const Track = require('./models/Track');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOption);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    const [oxxxymiron, guf, basta] = await Artist.create (
        {name: 'Oxxxymiron', photo: 'oxxxymiron.jpg', info: 'рэп-, грайм-, фристайл-исполнитель'},
        {name: 'GUF', photo: 'guf.jpg', info: 'российский рэп-исполнитель, сооснователь и участник группы CENTR'},
        {name: 'Basta', photo: 'basta.jpg', info: 'российский музыкант, телерадиоведущий, актёр, сценарист, режиссёр и продюсер.'}
    );

    const [gorgorod, bastaGuf, basta4] = await Album.create (
        {name: 'Горгород', photo: 'gorgorod.jpg', date: '13.11.2015', artist: oxxxymiron._id},
        {name: 'Баста/Гуф', photo: 'bastaguf.jpg', date: '10.11.2010', artist: guf._id},
        {name: 'Баста 4', photo: 'chk.jpg', date: '20.04.2013', artist: basta._id}
    );

    await Track.create(
        {
            name: 'Переплетено',
            time: '4:51',
            album: gorgorod._id
        },
        {
            name: 'Слова мэра',
            time: '4:01',
            album: gorgorod._id
        },
        {
            name: 'Заколоченное',
            time: '4:09',
            album: bastaGuf._id
        },
        {
            name: 'Чайный пьяница',
            time: '5:15',
            album: bastaGuf._id
        },
        {
            name: 'Чистый кайф',
            time: '6:05',
            album: basta4._id
        },
        {
            name: 'Пуэрчик покрепче',
            time: '4:54',
            album: basta4._id
        }
    );

    return connection.close();
};

run().catch(error => {
    console.log('Oops something happened...', error);
});