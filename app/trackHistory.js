const express = require('express');

const User = require('../models/Users');
const TrackHistory = require('../models/TrackHistory');

const router  = express.Router();

router.post('/', async (req, res) => {
    const token = await req.get("Token");

    const user = await User.findOne({token});

    if (!token) {
        return res.status(401).send({error: 'Unauthorized'});
    }

    if (!user) {
        return res.status(401).send({error: 'Unauthorized'});
    }

    const datetime = new Date().toISOString();

    const  trackId = ({user: user._id, track: req.body.track, datetime});

    const historyTrack = await new  TrackHistory(trackId);

    historyTrack.save()
        .then(track => res.send(track))
        .catch(() => res.sendStatus(400));
    res.send(trackId);
});

module.exports = router;